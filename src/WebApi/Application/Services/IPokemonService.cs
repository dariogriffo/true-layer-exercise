﻿using WebApi.Domain;

namespace WebApi.Application.Services;

public interface IPokemonService
{
    Task<Pokemon> GetByName(string name, CancellationToken cancellationToken);
}