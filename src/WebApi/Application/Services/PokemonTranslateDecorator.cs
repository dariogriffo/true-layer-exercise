﻿using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using WebApi.Domain;
using WebApi.Infrastructure;

namespace WebApi.Application.Services;

public class PokemonTranslateDecorator : ITranslatedPokemonService
{
    private readonly HttpClient _client;
    private readonly IPokemonService _inner;

    private static readonly JsonSerializerSettings Settings = new()
    {
        ContractResolver = new DefaultContractResolver
        {
            NamingStrategy = new SnakeCaseNamingStrategy()
        }
    };

    public PokemonTranslateDecorator(IPokemonService inner, IHttpClientFactory factory)
    {
        _inner = inner;
        _client = factory.CreateClient(Constants.TranslatorHttpClientName);
    }


    public async Task<Pokemon> GetByName(string name, CancellationToken cancellationToken)
    {
        if (string.IsNullOrEmpty(name))
        {
            throw new ArgumentException("Argument cannot be empty", nameof(name));
        }

        var response = await _inner.GetByName(name, cancellationToken);

        return await GetTranslatedPokemon(response, cancellationToken);
    }

    private async Task<Pokemon> GetTranslatedPokemon(Pokemon pokemon, CancellationToken cancellationToken)
    {
        if (string.IsNullOrEmpty(pokemon.Description))
        {
            return pokemon;
        }
        
        var translation = pokemon.IsLegendary || pokemon.Habitat == "rare" ? "yoda" : "shakespeare";
        
        //We do this because the api doesn't like non-visible characters
        var sanitizedDescription = Regex.Replace(pokemon.Description, @"\p{C}+", " ");
        var message =
            await _client.GetAsync($"/translate/{translation}.json?text={sanitizedDescription}", cancellationToken);
        
        if (!message.IsSuccessStatusCode)
        {
            return pokemon;
        }
        
        var content = await message.Content.ReadAsStringAsync(cancellationToken);
        var response = JsonConvert.DeserializeObject<TranslationResponse>(content, Settings);
        if (response.Success.Total == 1)
        {
            pokemon.SetDescription(response.Contents.Translated);
        }

        return pokemon;
    }
}
