﻿using System.Diagnostics.CodeAnalysis;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using WebApi.Domain;
using WebApi.Infrastructure;
using WebApi.Infrastructure.Exceptions;
using WebApi.Infrastructure.PokemonApi.Models;

namespace WebApi.Application.Services;

public class PokemonService : IPokemonService
{
    private readonly HttpClient _client;

    private static readonly JsonSerializerSettings Settings = new()
    {
        ContractResolver = new DefaultContractResolver
        {
            NamingStrategy = new SnakeCaseNamingStrategy()
        }
    };

    public PokemonService(IHttpClientFactory factory)
    {
        _client = factory.CreateClient(Constants.PokemonApiHttpClientName);
    }

    public async Task<Pokemon> GetByName(
        [NotNull] string name,
        CancellationToken cancellationToken)
    {
        if (string.IsNullOrEmpty(name))
        {
            throw new ArgumentException("Argument cannot be empty", nameof(name));
        }

        var message = await _client.GetAsync($"/api/v2/pokemon/{name}", cancellationToken);
        if (!message.IsSuccessStatusCode)
        {
            if (message.StatusCode != HttpStatusCode.NotFound)
            {
                throw new DownstreamServiceException();
            }

            throw new PokemonNotFoundException();
        }

        var content = await message.Content.ReadAsStringAsync(cancellationToken);
        var pokemon = JsonConvert.DeserializeObject<PokemonResponse>(content, Settings);
        message = await _client.GetAsync(pokemon!.Species.Url, cancellationToken);

        if (!message.IsSuccessStatusCode && message.StatusCode != HttpStatusCode.NotFound)
        {
            throw new DownstreamServiceException();
        }

        content = await message.Content.ReadAsStringAsync(cancellationToken);
        var species = JsonConvert.DeserializeObject<SpeciesResponse>(content, Settings);

        var description = string.Empty;
        if (species!.FlavorText.Any())
        {
            description = species!.FlavorText[0].FlavorText;
        }

        return new Pokemon(pokemon.Name, description, species.Habitat.Name, species.IsLegendary);
    }
}
