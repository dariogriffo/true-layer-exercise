﻿namespace WebApi.Domain;

public class Pokemon
{
    public Pokemon(string name, string description, string habitat, bool isLegendary)
    {
        Name = name;
        Description = description;
        Habitat = habitat;
        IsLegendary = isLegendary;
    }
    
    public string Name { get; }
    public string Description { get; private set; }
    public string Habitat { get; }
    public bool IsLegendary { get; }

    public void SetDescription(string description)
    {
        Description = description;
    }
}
