﻿using Newtonsoft.Json;

namespace WebApi.Infrastructure.PokemonApi.Models;

public class Language
{
    [JsonConstructor]
    public Language(
        string name,
        string url
    )
    {
        Name = name;
        Url = url;
    }

    public string Name { get; }

    public string Url { get; }
}