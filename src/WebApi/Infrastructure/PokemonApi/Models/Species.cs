﻿using Newtonsoft.Json;

namespace WebApi.Infrastructure.PokemonApi.Models;

public class Species
{
    [JsonConstructor]
    public Species(
        string name,
        string url
    )
    {
        Name = name;
        Url = url;
    }

    public string Name { get; }

    public string Url { get; }
}