﻿using Newtonsoft.Json;

namespace WebApi.Infrastructure.PokemonApi.Models;

public class Version
{
    [JsonConstructor]
    public Version(
        string name,
        string url
    )
    {
        this.Name = name;
        this.Url = url;
    }

    public string Name { get; }

    public string Url { get; }
}