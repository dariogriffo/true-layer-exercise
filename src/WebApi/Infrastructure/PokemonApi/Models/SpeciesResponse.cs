﻿using Newtonsoft.Json;

namespace WebApi.Infrastructure.PokemonApi.Models;

public class SpeciesResponse
{
    [JsonConstructor]
    public SpeciesResponse(
        List<FlavorTextEntry> flavorTextEntries,
        Habitat habitat,
        bool isLegendary
    )
    {
        FlavorText = flavorTextEntries;
        Habitat = habitat;
        IsLegendary = isLegendary;
    }

    public IReadOnlyList<FlavorTextEntry> FlavorText { get; }
    
    public Habitat Habitat { get; }

    public bool IsLegendary { get; }
}
