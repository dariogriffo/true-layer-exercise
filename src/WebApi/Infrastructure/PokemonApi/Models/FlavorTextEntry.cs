﻿using Newtonsoft.Json;

namespace WebApi.Infrastructure.PokemonApi.Models;

public class FlavorTextEntry
{
    [JsonConstructor]
    public FlavorTextEntry(
        string flavorText,
        Language language,
        Version version
    )
    {
        FlavorText = flavorText;
        Language = language;
        Version = version;
    }

    public string FlavorText { get; }

    public Language Language { get; }

    public Version Version { get; }
}
