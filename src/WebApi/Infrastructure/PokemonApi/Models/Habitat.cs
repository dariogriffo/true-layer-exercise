﻿using Newtonsoft.Json;

namespace WebApi.Infrastructure.PokemonApi.Models;

public class Habitat
{
    [JsonConstructor]
    public Habitat(
        string name,
        string url
    )
    {
        this.Name = name;
        this.Url = url;
    }

    public string Name { get; }

    public string Url { get; }
}