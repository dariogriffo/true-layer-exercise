﻿using Newtonsoft.Json;

namespace WebApi.Infrastructure.PokemonApi.Models;

public class PokemonResponse
{
    [JsonConstructor]
    public PokemonResponse(
        string name,
         Species species
    )
    {
        Name = name;
        Species = species;
    }

    public string Name { get; }

    public Species Species { get; }
}
