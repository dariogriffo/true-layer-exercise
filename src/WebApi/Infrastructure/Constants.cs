﻿namespace WebApi.Infrastructure;

public static class Constants
{
    public const string PokemonApiHttpClientName = "poke-api";
    public const string TranslatorHttpClientName = "translator-api";
}
