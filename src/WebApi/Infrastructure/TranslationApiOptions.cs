﻿namespace WebApi.Infrastructure;

public class TranslationApiOptions
{
    public string Url { get; set; } = "https://api.funtranslations.com/";
}