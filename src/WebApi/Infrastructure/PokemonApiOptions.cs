﻿namespace WebApi.Infrastructure;

public class PokemonApiOptions
{
    public string Url { get; set; } = "https://pokeapi.co/";
}