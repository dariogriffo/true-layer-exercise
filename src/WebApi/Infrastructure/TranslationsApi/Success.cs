﻿using Newtonsoft.Json;

public class Success
{
    [JsonConstructor]
    public Success(
        int total
    )
    {
        Total = total;
    }

    public int Total { get; }
}