﻿using Newtonsoft.Json;

public class Contents
{
    [JsonConstructor]
    public Contents(
        string translated,
        string text,
        string translation
    )
    {
        Translated = translated;
        Text = text;
        Translation = translation;
    }

    public string Translated { get; }

    public string Text { get; }

    public string Translation { get; }
}