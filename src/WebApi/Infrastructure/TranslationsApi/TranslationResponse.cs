﻿using Newtonsoft.Json;

public class TranslationResponse
{
    [JsonConstructor]
    public TranslationResponse(
        Success success,
        Contents contents
    )
    {
        Success = success;
        Contents = contents;
    }

    public Success Success { get; }
    
    public Contents Contents { get; }
}