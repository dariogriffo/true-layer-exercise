﻿using ServiceCollection.Extensions.Modules;
using Polly;
using Polly.Contrib.WaitAndRetry;
using Polly.Extensions.Http;
using WebApi.Application.Services;
using WebApi.Infrastructure;

namespace WebApi;

public class ApiModule : Module
{
    protected override void Load(IServiceCollection services)
    {
        services.AddSingleton<IPokemonService, PokemonService>();
        services.AddSingleton<PokemonApiOptions>();
        services.AddSingleton<TranslationApiOptions>();
        services.AddSingleton<ITranslatedPokemonService, PokemonTranslateDecorator>();

        //These values should be picked from Configuration and probably different for each API
        var delay =
            Backoff
                .DecorrelatedJitterBackoffV2(medianFirstRetryDelay: TimeSpan.FromSeconds(1), retryCount: 5)
                .ToArray();

        services.AddHttpClient(Constants.PokemonApiHttpClientName, (serviceProvider, client) =>
        {
            var options = serviceProvider
                              .GetRequiredService<PokemonApiOptions>() ??
                          throw new ArgumentNullException(nameof(PokemonApiOptions));
            client.BaseAddress = new Uri($"{options.Url.TrimEnd('/')}");
        }).AddPolicyHandler(
            HttpPolicyExtensions
                .HandleTransientHttpError()
                .OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.ServiceUnavailable)
                .WaitAndRetryAsync(delay));


        services.AddHttpClient(Constants.TranslatorHttpClientName, (serviceProvider, client) =>
        {
            var options = serviceProvider
                              .GetRequiredService<TranslationApiOptions>() ??
                          throw new ArgumentNullException(nameof(TranslationApiOptions));
            client.BaseAddress = new Uri($"{options.Url.TrimEnd('/')}");
        }).AddPolicyHandler(
            HttpPolicyExtensions
                .HandleTransientHttpError()
                .OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.ServiceUnavailable)
                .WaitAndRetryAsync(delay));
    }
}