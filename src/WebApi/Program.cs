using ServiceCollection.Extensions.Modules;
using WebApi;
using WebApi.Application.Services;
using WebApi.Domain;
using WebApi.Infrastructure.Exceptions;

var builder = WebApplication.CreateBuilder(args);

builder.Services.RegisterModule<ApiModule>();

var app = builder.Build();

async Task<Pokemon?> GetPokemonByName(IPokemonService pokemonService, string name, HttpContext context)
{
    try
    {
        return await pokemonService.GetByName(name, context.RequestAborted);
    }
    catch (ArgumentException)
    {
        context.Response.StatusCode = 400;
        return default;
    }
    catch (PokemonNotFoundException)
    {
        context.Response.StatusCode = 404;
        return default;
    }
    catch (DownstreamServiceException)
    {
        context.Response.StatusCode = 503;
        return default;
    }
}

Task<Pokemon?> GetTranslatedPokemonByName(ITranslatedPokemonService pokemonService, string name, HttpContext context) =>
    GetPokemonByName(pokemonService, name, context);

app.MapGet("/pokemon/{name}", GetPokemonByName);
app.MapGet("/pokemon/translated/{name}", GetTranslatedPokemonByName);

app.Run();