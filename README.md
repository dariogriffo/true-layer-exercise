
# Requirements

- Docker from [here](https://docs.docker.com/get-docker/)
- Visual Studio/VS code/JetBrains Rider to easily navigate the code
- dotnet sdk 6.0

## Running the api

- Open a command line on this directory and run docker compose up
- At this moment the app is running and a postman collection ran, so please verify the output of the console

## Running requests manually

If nothing changed in the docker-compose.yml file the api is running on http://localhost:8080
To run requests manually curl is required but is pre-installed with git

- Open a bash terminal and run

```bash
curl --location --request GET 'http://localhost:8080/pokemon/ditto'
curl --location --request GET 'http://localhost:8080/pokemon/translated/ditto'
```

# To-Do

I would have used more time to
- Add better logging
- Add metrics
- Better error handling (CancellationRequested exception, invalid characters)

# Production changes

I would change the endpoint #2 path to

`/pokemon/{name}/translated`

to be aligned with a REST api url
