﻿using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebApi.Application.Services;
using WebApi.Domain;
using WebApi.Infrastructure;

namespace WebApi.UnitTests.Helpers;

class PokemonApiFactory : WebApplicationFactory<Pokemon>
{
    protected override IHost CreateHost(IHostBuilder builder)
    {
        builder.ConfigureServices(services =>
        {
            services.AddSingleton<IPokemonService, FakePokemonService>();
            services.AddSingleton<ITranslatedPokemonService, FakeTranslatorService>();
        });
        return base.CreateHost(builder);
    }

    public override ValueTask DisposeAsync()
    {
        return ValueTask.CompletedTask;
    }
}
