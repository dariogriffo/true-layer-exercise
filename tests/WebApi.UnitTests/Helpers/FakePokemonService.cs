﻿using WebApi.Application.Services;
using WebApi.Domain;
using WebApi.Infrastructure;

namespace WebApi.UnitTests.Helpers;

internal class FakePokemonService : IPokemonService
{
    internal static Pokemon MewtwoPokemon = new(
        "mewtwo",
        "It was created by\na scientist after\nyears of horrific\fgene splicing and\nDNA engineering\nexperiments.",
        "rare", 
        true);
    
    public Task<Pokemon> GetByName(string name, CancellationToken cancellationToken)
    {
        return Task.FromResult(MewtwoPokemon);
    }
}