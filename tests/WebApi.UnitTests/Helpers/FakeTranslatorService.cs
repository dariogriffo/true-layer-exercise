﻿using WebApi.Application.Services;
using WebApi.Domain;
using WebApi.Infrastructure;

namespace WebApi.UnitTests.Helpers;

internal class FakeTranslatorService : ITranslatedPokemonService
{internal static Pokemon MewtwoPokemon = new(
        "mewtwo",
        "Created by\\na scientist after\\nyears of horrific\\fgene splicing and\\ndna engineering\\nexperiments,  it was.",
        "rare", 
        true);
    
    public Task<Pokemon> GetByName(string name, CancellationToken cancellationToken)
    {
        return Task.FromResult(MewtwoPokemon);
    }
}
