﻿using FluentAssertions;
using Moq;
using RichardSzalay.MockHttp;
using WebApi.Application.Services;
using WebApi.Domain;
using WebApi.Infrastructure;
using Xunit;

namespace WebApi.UnitTests;

public class PokemonServiceTests
{
    private static readonly PokemonApiOptions Options = new PokemonApiOptions();
    
    [Fact]
    public async Task GetByName_When_Name_Is_Empty_Throws_Argument_Exception()
    {
        var handlerMock = new MockHttpMessageHandler();
        var client = new HttpClient(handlerMock) { BaseAddress = new Uri(Options.Url) };
        var factory = Mock.Of<IHttpClientFactory>();
        Mock.Get(factory).Setup(x => x.CreateClient(Constants.PokemonApiHttpClientName)).Returns(client);
        var sut = new PokemonService(factory);
        Func<Task> act = async () => await sut.GetByName(string.Empty, CancellationToken.None);
        await act.Should().ThrowAsync<ArgumentException>().WithMessage("Argument cannot be empty (Parameter 'name')");
    }

    [Fact]
    public async Task GetByName_When_Name_Valid_Returns_Valid_Pokemon()
    {
        var handlerMock = new MockHttpMessageHandler();
        var expected = new Pokemon(
            "mewtwo",
            "It was created by\na scientist after\nyears of horrific\fgene splicing and\nDNA engineering\nexperiments.",
            "rare",
            true);
        
        const string pokeResponse = "{\"name\":\"mewtwo\",\"species\":{\"name\":\"mewtwo\",\"url\":\"https://pokeapi.co/api/v2/pokemon-species/150/\"}}";
        const string speciesResponse =
            "{\"flavor_text_entries\": [{\"flavor_text\": \"It was created by\na scientist after\nyears of horrific\fgene splicing and\nDNA engineering\nexperiments.\",\"language\": {\"name\": \"en\",\"url\": \"https://pokeapi.co/api/v2/language/9/\"},\"version\": {\"name\": \"red\",\"url\": \"https://pokeapi.co/api/v2/version/1/\"}}],\"habitat\": {\"name\": \"rare\",\"url\": \"https://pokeapi.co/api/v2/pokemon-habitat/5/\"},\"is_legendary\": true}";
        handlerMock
            .When(HttpMethod.Get, "/api/v2/pokemon/mewtwo")
            .Respond("application/json", pokeResponse);
        
        handlerMock
            .When(HttpMethod.Get, "/api/v2/pokemon-species/150/")
            .Respond("application/json", speciesResponse);
        
        var client = new HttpClient(handlerMock) { BaseAddress = new Uri(Options.Url) };
        
        var factory = Mock.Of<IHttpClientFactory>();
        Mock.Get(factory).Setup(x => x.CreateClient(Constants.PokemonApiHttpClientName)).Returns(client);
        var sut = new PokemonService(factory);
        var response = await sut.GetByName("mewtwo", CancellationToken.None);

        response.Should().BeEquivalentTo(expected);
    }
}
