﻿using System.Net.Http.Json;
using FluentAssertions;
using WebApi.Domain;
using WebApi.UnitTests.Helpers;
using Xunit;

namespace WebApi.UnitTests;

public class EndpointTests
{
    [Fact]
    public async Task GetMetwo_Should_Not_Return_Null()
    {
        await using var application = new PokemonApiFactory();

        var client = application.CreateClient();
        var response = await client.GetFromJsonAsync<Pokemon>("/pokemon/mewtwo");

        response.Should().BeEquivalentTo(FakePokemonService.MewtwoPokemon);
    }

    [Fact]
    public async Task GetMetwoTranslated_Should_Not_Return_Null()
    {
        var expected = new Pokemon(
            "mewtwo",
            "Created by\\na scientist after\\nyears of horrific\\fgene splicing and\\ndna engineering\\nexperiments,  it was.",
            "rare",
            true);
        
        await using var application = new PokemonApiFactory();

        var client = application.CreateClient();
        var response = await client.GetFromJsonAsync<Pokemon>("/pokemon/translated/mewtwo");

        response.Should().BeEquivalentTo(expected);
    }
}
