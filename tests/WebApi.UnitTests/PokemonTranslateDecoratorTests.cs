﻿using FluentAssertions;
using Moq;
using RichardSzalay.MockHttp;
using WebApi.Application.Services;
using WebApi.Domain;
using WebApi.Infrastructure;
using Xunit;

namespace WebApi.UnitTests;

public class PokemonTranslateDecoratorTests
{
    private static readonly TranslationApiOptions TranslationApiOptions = new TranslationApiOptions();

    [Fact]
    public async Task GetByName_When_Name_Is_Empty_Throws_Argument_Exception()
    {
        var handlerMock = new MockHttpMessageHandler();
        var pokemonService = Mock.Of<IPokemonService>();
        var factory = Mock.Of<IHttpClientFactory>();
        var client = new HttpClient(handlerMock) { BaseAddress = new Uri(TranslationApiOptions.Url) };
        Mock.Get(factory).Setup(x => x.CreateClient(Constants.TranslatorHttpClientName)).Returns(client);
        var sut = new PokemonTranslateDecorator(pokemonService, factory);
        Func<Task> act = async () => await sut.GetByName(string.Empty, CancellationToken.None);
        await act.Should().ThrowAsync<ArgumentException>().WithMessage("Argument cannot be empty (Parameter 'name')");
    }

    [Fact]
    public async Task GetByName_When_Pokemon_Habitat_Is_Rare_Valid_Returns_Yoda_Translated_Pokemon()
    {
        var expected = new Pokemon(
            "mewtwo",
            "Mr,  you gave.Tim a hearty meal,Made him die,  but unfortunately what he ate.",
            "rare",
            true);
        
        var handlerMock = new MockHttpMessageHandler();
        var pokemonService = Mock.Of<IPokemonService>();
        Mock
            .Get(pokemonService)
            .Setup(x => x.GetByName(It.IsAny<string>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(new Pokemon(
                "mewtwo",
                "It was created by\na scientist after\nyears of horrific\fgene splicing and\nDNA engineering\nexperiments.",
                "rare",
                true));
        
        var factory = Mock.Of<IHttpClientFactory>();
        
        const string translationResponse = "{\"success\": {\"total\": 1},\"contents\": {\"translated\": \"Mr,  you gave.Tim a hearty meal,Made him die,  but unfortunately what he ate.\",\"text\": \"You gave Mr. Tim a hearty meal, but unfortunately what he ate made him die.\",\"translation\": \"yoda\"}}";
        handlerMock
            .When(HttpMethod.Get, "/translate/yoda.json")
            .Respond("application/json", translationResponse);
        
        var client = new HttpClient(handlerMock) { BaseAddress = new Uri(TranslationApiOptions.Url) };
        Mock.Get(factory).Setup(x => x.CreateClient(Constants.TranslatorHttpClientName)).Returns(client);
        var sut = new PokemonTranslateDecorator(pokemonService, factory);
        var response = await sut.GetByName("mewtwo", CancellationToken.None);

        response.Should().BeEquivalentTo(expected);
    }

    [Fact]
    public async Task GetByName_When_Pokemon_Is_Legendary_Valid_Returns_Yoda_Translated_Pokemon()
    {
        var expected = new Pokemon(
            "mewtwo",
            "Mr,  you gave.Tim a hearty meal,Made him die,  but unfortunately what he ate.",
            "rare",
            true);
        
        var handlerMock = new MockHttpMessageHandler();
        var pokemonService = Mock.Of<IPokemonService>();
        Mock
            .Get(pokemonService)
            .Setup(x => x.GetByName(It.IsAny<string>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(new Pokemon(
                "mewtwo",
                "It was created by\na scientist after\nyears of horrific\fgene splicing and\nDNA engineering\nexperiments.",
                "rare",
                true));
        
        var factory = Mock.Of<IHttpClientFactory>();
        
        const string translationResponse = "{\"success\": {\"total\": 1},\"contents\": {\"translated\": \"Mr,  you gave.Tim a hearty meal,Made him die,  but unfortunately what he ate.\",\"text\": \"You gave Mr. Tim a hearty meal, but unfortunately what he ate made him die.\",\"translation\": \"yoda\"}}";
        handlerMock
            .When(HttpMethod.Get, "/translate/yoda.json")
            .Respond("application/json", translationResponse);
        
        var client = new HttpClient(handlerMock) { BaseAddress = new Uri(TranslationApiOptions.Url) };
        Mock.Get(factory).Setup(x => x.CreateClient(Constants.TranslatorHttpClientName)).Returns(client);
        var sut = new PokemonTranslateDecorator(pokemonService, factory);
        var response = await sut.GetByName("mewtwo", CancellationToken.None);

        response.Should().BeEquivalentTo(expected);
    }

    [Fact]
    public async Task GetByName_When_Pokemon_Is_Not_Legendary_And_Habitat_Is_Not_Rare_Returns_Shakesperian_Translated_Pokemon()
    {
        var expected = new Pokemon(
            "ditto",
            "Capable of copying\nan foe's genetic\ncode to instantly\ftransform itself\ninto a duplicate\nof the foe.",
            "urban",
            false);
        
        var handlerMock = new MockHttpMessageHandler();
        var pokemonService = Mock.Of<IPokemonService>();
        Mock
            .Get(pokemonService)
            .Setup(x => x.GetByName(It.IsAny<string>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(new Pokemon(
                "ditto",
                "Capable of copying\nan enemy's genetic\ncode to instantly\ftransform itself\ninto a duplicate\nof the enemy.",
                "urban",
                false));
        
        var factory = Mock.Of<IHttpClientFactory>();
        
        const string translationResponse = "{\"success\": {\"total\": 1},\"contents\": {\"translated\": \"Capable of copying\\nan foe's genetic\\ncode to instantly\\ftransform itself\\ninto a duplicate\\nof the foe.\",\"text\": \"You gave Mr. Tim a hearty meal, but unfortunately what he ate made him die.\",\"translation\": \"shakespeare\"}}";
        handlerMock
            .When(HttpMethod.Get, "/translate/shakespeare.json")
            .Respond("application/json", translationResponse);
        
        var client = new HttpClient(handlerMock) { BaseAddress = new Uri(TranslationApiOptions.Url) };
        Mock.Get(factory).Setup(x => x.CreateClient(Constants.TranslatorHttpClientName)).Returns(client);
        var sut = new PokemonTranslateDecorator(pokemonService, factory);
        var response = await sut.GetByName("ditto", CancellationToken.None);

        response.Should().BeEquivalentTo(expected);
    }
}
